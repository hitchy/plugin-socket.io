"use strict";

module.exports = function() {
	/**
	 * Implements policy handlers for server-side socket.io endpoint.
	 */
	class SocketPolicies {
		/**
		 * Stops request processing by demanding to ignore any follow-up handler
		 * matching route of incoming request in case it's addressing the realm
		 * of server-side websocket.
		 *
		 * @param {Hitchy.Core.IncomingMessage} req request descriptor
		 * @param {Hitchy.Core.ServerResponse} _ response manager
		 * @param {Hitchy.Core.ContinuationHandler} next callback to invoke when handler is done
		 * @returns {void}
		 */
		static ignore( req, _, next ) {
			if ( req.url.startsWith( "/socket.io/" ) ) {
				req.ignore();
			}

			next();
		}
	}

	return SocketPolicies;
};

"use strict";

const { Server } = require( "socket.io" );

const ptnJson = /^(?:application|text)\/json(?:;|$)/;

module.exports = function() {
	const api = this;
	const { config, services } = api;

	const logDebug = api.log( "hitchy:socket:debug" );
	const logError = api.log( "hitchy:socket:error" );

	let started = false;

	const namespaces = new Map();

	/**
	 * Implements API for interacting with websocket connections of clients.
	 */
	class SocketServer {
		/**
		 * Initializes socket server stub.
		 *
		 * @returns {void}
		 */
		static start() {
			if ( !started ) {
				started = true;

				logDebug( "preparing socket.io server to integrate with HTTP server when available" );

				api.once( "assign-server", httpServer => {
					logDebug( "attaching socket.io server to HTTP server" );

					const io = new Server( config.socket.port || httpServer, {
						serveClient: true,
						...config.socket,
					} );

					Object.defineProperty( this, "io", { value: io } );

					this.collectNamespaces( io );
					this.startRequestDispatcher( io );

					api.emit( "websocket", io );
				} );

				api.once( "shutdown", () => {
					setTimeout( () => {
						const server = api.websocket;

						if ( server ) {
							for ( const namespace of namespaces.keys() ) {
								logDebug( `disconnecting sockets of namespace ${namespace}` );
								server.of( namespace ).disconnectSockets( true );
							}

							logDebug( `disconnecting sockets of global namespace` );
							server.disconnectSockets( true );

							logDebug( "closing server" );
							server.close( () => {
								logDebug( "removing all listeners on server" );
								server.removeAllListeners();
							} );
						}
					}, 50 );
				} );
			}
		}

		/**
		 * Sets up listener for tracking namespaces used on server side so that
		 * they can be released individually on shutting down.
		 *
		 * @param {Server} io websocket server
		 * @returns {void}
		 */
		static collectNamespaces( io ) {
			io.on( "new_namespace", namespace => {
				namespaces.set( namespace.name, namespace );
			} );
		}

		/**
		 * Registers listener for dispatching routed requests for Hitchy
		 * policies and controllers.
		 *
		 * @param {Server} io websocket server
		 * @returns {void}
		 */
		static startRequestDispatcher( io ) {
			io.of( "/hitchy" ).on( "connection", socket => {
				socket.on( "request", async( { method, url, headers, body } = {}, responder ) => {
					try {
						if ( !/^GET|POST|PUT|PATCH|DELETE|HEAD|OPTIONS|TRACE$/i.test( method ) ) {
							throw new services.HttpException( 400, `invalid HTTP request method: ${method}` );
						}

						if ( !/^\/(?:[^/\s]|$)/.test( url ) ) {
							throw new services.HttpException( 400, `HTTP request URL must be local absolute path, got ${url}` );
						}

						if ( headers != null && ( typeof headers !== "object" || Object.getPrototypeOf( headers ) !== Object.prototype ) ) {
							throw new services.HttpException( 400, `HTTP request headers must be Object, got ${typeof headers}` );
						}

						// fix headers
						const lowerCaseHeaders = {};

						for ( const name of Object.keys( headers ) ) {
							lowerCaseHeaders[name.toLowerCase()] = headers[name];
						}

						if ( body != null && typeof body !== "string" && !Buffer.isBuffer( body ) ) {
							const type = lowerCaseHeaders["content-type"];

							if ( type ) {
								if ( !ptnJson.test( type ) ) {
									throw new services.HttpException( 400, `HTTP request has JSON body, but mismatching content-type header ${type}` );
								}
							} else {
								lowerCaseHeaders["content-type"] = "application/json";
							}
						}

						// start dispatching request
						const client = new api.Client( {
							method, url, headers: lowerCaseHeaders
						} );

						if ( body == null ) {
							client.end();
						} else if ( typeof body === "string" ) {
							client.end( body, "utf8" );
						} else if ( Buffer.isBuffer( body ) ) {
							client.end( body );
						} else {
							client.end( JSON.stringify( body ), "utf8" );
						}

						// handle response
						const response = await client.dispatch();
						const responseHeaders = response.getHeaders();
						let responseBody = await response.body();

						if ( ptnJson.test( responseHeaders["content-type"] ) ) {
							responseBody = JSON.parse( responseBody.toString( "utf8" ) );
						}

						// send response
						responder( {
							statusCode: response.statusCode,
							headers: responseHeaders,
							body: responseBody,
						} );
					} catch ( error ) {
						logError( error.stack );

						responder( {
							statusCode: parseInt( error.status || error.statusCode || error.code ) || 500,
							headers: {
								"content-type": "application/json",
							},
							body: {
								error: error.message,
							},
						} );
					}
				} );
			} );
		}
	}

	return SocketServer;
};

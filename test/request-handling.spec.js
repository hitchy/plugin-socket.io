"use strict";

const { describe, it, beforeEach, afterEach } = require( "mocha" );
const SDT = require( "@hitchy/server-dev-tools" );
const { io } = require( "socket.io-client" );

require( "should" );
require( "should-http" );

describe( "A Hitchy service featuring cookie-based sessions and server-side websocket", function() {
	this.timeout( 4000 );

	const ctx = {};

	afterEach( SDT.after( ctx ) );
	beforeEach( SDT.before( ctx, {
		plugin: true,
		options: {
			logger: true,  // must be true to collect produced logs for inspection
		},
		files: {}
	} ) );

	beforeEach( () => {
		ctx.url = `http://127.0.0.1:${ctx.server.address().port}`;
	} );

	it( "does not consider policies on raw websocket requests", async() => {
		const data = await new Promise( ( resolve, reject ) => {
			const socket = io( ctx.url );

			socket.once( "foo-to-all", resolve );
			socket.once( "connect_error", reject );

			socket.once( "connect", () => {
				ctx.hitchy.api.websocket.emit( "foo-to-all", { message: "foo!" } );
			} );
		} );

		data.should.be.Object().which.has.size( 1 ).and.has.property( "message" ).which.is.a.String().and.is.equal( "foo!" );
		ctx.logged.some( msg => msg.startsWith( "ERROR: " ) && msg.includes( "Cannot set headers after they are sent" ) ).should.be.false( "unexpected logging about issue with headers being sent prematurely" );
	} );
} );

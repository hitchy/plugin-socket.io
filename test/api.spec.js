"use strict";

const HTTP = require( "http" );
const { describe, it, before, after } = require( "mocha" );
const SDT = require( "@hitchy/server-dev-tools" );
const { io } = require( "socket.io-client" );

require( "should" );
require( "should-http" );


describe( "A Hitchy service for managing a server-side websocket", () => {
	describe( "when integrated with the HTTP server", function() {
		const ctx = {};

		after( SDT.after( ctx ) );
		before( SDT.before( ctx, {
			plugin: true,
			options: {
				// debug: true,
			},
			files: {
				"config/routes.js": `exports.routes = { 
					"ALL /foo/bar": async( req, res ) => res.json( { requestMethod: req.method, requestBody: await req.fetchBody(), requestHeaders: req.headers } ),
				};`,
			}
		} ) );

		before( () => {
			ctx.url = `http://127.0.0.1:${ctx.server.address().port}`;
		} );


		it( "is exposed at api.websocket", () => {
			ctx.hitchy.api.should.have.property( "websocket" );
			ctx.hitchy.api.websocket.should.be.Object();
		} );

		it( "is exposed at api.services.SocketServer.io", () => {
			ctx.hitchy.api.services.should.have.property( "SocketServer" );
			ctx.hitchy.api.websocket.should.be.equal( ctx.hitchy.api.services.SocketServer.io );
		} );

		it( "looks like the API of socket.io", () => {
			ctx.hitchy.api.websocket.on.should.be.Function();
			ctx.hitchy.api.websocket.of.should.be.Function();
			ctx.hitchy.api.websocket.socketsJoin.should.be.Function();
			ctx.hitchy.api.websocket.socketsLeave.should.be.Function();
			ctx.hitchy.api.websocket.serverSideEmit.should.be.Function();
		} );

		it( "accepts connection", async() => {
			await new Promise( ( resolve, reject ) => {
				const socket = io( ctx.url );

				socket.once( "connect_error", reject );
				socket.once( "connect", resolve );
			} );
		} );

		describe( "dispatches requests received via websocket which", () => {
			it( "succeeds", async() => {
				const result = await new Promise( ( resolve, reject ) => {
					const socket = io( ctx.url + "/hitchy" );

					socket.once( "connect_error", reject );

					socket.once( "connect", () => {
						socket.emit( "request", {
							method: "POST",
							url: "/foo/bar",
							headers: {},
							body: { input: "bar" },
						}, res => {
							resolve( {
								resultStatus: res.statusCode,
								resultHeaders: res.headers,
								resultBody: res.body,
							} );
						} );
					} );
				} );

				result.should.be.Object().which.has.size( 3 ).and.has.properties( "resultStatus", "resultHeaders", "resultBody" );
				result.resultStatus.should.be.equal( 200 );
				result.resultHeaders.should.be.Object().which.has.property( "content-type" ).which.startWith( "application/json" );
				result.resultBody.should.be.Object().which.has.size( 3 ).and.has.properties( "requestMethod", "requestBody", "requestHeaders" );
				result.resultBody.requestMethod.should.be.String().which.is.equal( "POST" );
				result.resultBody.requestBody.should.be.Object().which.has.size( 1 ).and.has.property( "input" ).which.is.a.String().and.is.equal( "bar" );
			} );

			it( "passes custom request headers", async() => {
				const result = await new Promise( ( resolve, reject ) => {
					const socket = io( ctx.url + "/hitchy" );

					socket.once( "connect_error", reject );

					socket.once( "connect", () => {
						socket.emit( "request", {
							method: "POST",
							url: "/foo/bar",
							headers: {
								fOo: "text/json",
							},
							body: { input: "bar" },
						}, res => {
							resolve( {
								resultStatus: res.statusCode,
								resultHeaders: res.headers,
								resultBody: res.body,
							} );
						} );
					} );
				} );

				result.should.be.Object().which.has.size( 3 ).and.has.properties( "resultStatus", "resultHeaders", "resultBody" );
				result.resultStatus.should.be.equal( 200 );
				result.resultHeaders.should.be.Object().which.has.property( "content-type" ).which.startWith( "application/json" );
				result.resultBody.should.be.Object().which.has.size( 3 ).and.has.properties( "requestMethod", "requestBody", "requestHeaders" );
				result.resultBody.requestMethod.should.be.String().which.is.equal( "POST" );
				result.resultBody.requestBody.should.be.Object().which.has.size( 1 ).and.has.property( "input" ).which.is.a.String().and.is.equal( "bar" );
				result.resultBody.requestHeaders.should.be.Object().which.has.property( "foo" ).which.is.a.String().and.is.equal( "text/json" );
			} );

			it( "fails on using unsupported HTTP method", async() => {
				const result = await new Promise( ( resolve, reject ) => {
					const socket = io( ctx.url + "/hitchy" );

					socket.once( "connect_error", reject );

					socket.once( "connect", () => {
						socket.emit( "request", {
							method: "PROPFIND",
							url: "/foo/bar",
							headers: {},
							body: { input: "bar" },
						}, res => {
							resolve( {
								resultStatus: res.statusCode,
								resultHeaders: res.headers,
								resultBody: res.body,
							} );
						} );
					} );
				} );

				result.should.be.Object().which.has.size( 3 ).and.has.properties( "resultStatus", "resultHeaders", "resultBody" );
				result.resultStatus.should.be.equal( 400 );
				result.resultHeaders.should.be.Object().which.has.property( "content-type" ).which.startWith( "application/json" );
				result.resultBody.should.be.Object().which.has.size( 1 ).and.has.properties( "error" );
				result.resultBody.error.should.be.String();
			} );

			it( "fails on providing remote URL to fetch", async() => {
				const result = await new Promise( ( resolve, reject ) => {
					const socket = io( ctx.url + "/hitchy" );

					socket.once( "connect_error", reject );

					socket.once( "connect", () => {
						socket.emit( "request", {
							method: "POST",
							url: "https://google.com/foo/bar",
							headers: {},
							body: { input: "bar" },
						}, res => {
							resolve( {
								resultStatus: res.statusCode,
								resultHeaders: res.headers,
								resultBody: res.body,
							} );
						} );
					} );
				} );

				result.should.be.Object().which.has.size( 3 ).and.has.properties( "resultStatus", "resultHeaders", "resultBody" );
				result.resultStatus.should.be.equal( 400 );
				result.resultHeaders.should.be.Object().which.has.property( "content-type" ).which.startWith( "application/json" );
				result.resultBody.should.be.Object().which.has.size( 1 ).and.has.properties( "error" );
				result.resultBody.error.should.be.String();
			} );

			it( "fails on providing invalid header", async() => {
				const result = await new Promise( ( resolve, reject ) => {
					const socket = io( ctx.url + "/hitchy" );

					socket.once( "connect_error", reject );

					socket.once( "connect", () => {
						socket.emit( "request", {
							method: "POST",
							url: "/foo/bar",
							headers: "some header",
							body: { input: "bar" },
						}, res => {
							resolve( {
								resultStatus: res.statusCode,
								resultHeaders: res.headers,
								resultBody: res.body,
							} );
						} );
					} );
				} );

				result.should.be.Object().which.has.size( 3 ).and.has.properties( "resultStatus", "resultHeaders", "resultBody" );
				result.resultStatus.should.be.equal( 400 );
				result.resultHeaders.should.be.Object().which.has.property( "content-type" ).which.startWith( "application/json" );
				result.resultBody.should.be.Object().which.has.size( 1 ).and.has.properties( "error" );
				result.resultBody.error.should.be.String();
			} );

			it( "fails on providing regular object as payload combined with individual content-type request header", async() => {
				const result = await new Promise( ( resolve, reject ) => {
					const socket = io( ctx.url + "/hitchy" );

					socket.once( "connect_error", reject );

					socket.once( "connect", () => {
						socket.emit( "request", {
							method: "POST",
							url: "/foo/bar",
							headers: {
								"content-type": "application/octet-stream"
							},
							body: { input: "bar" },
						}, res => {
							resolve( {
								resultStatus: res.statusCode,
								resultHeaders: res.headers,
								resultBody: res.body,
							} );
						} );
					} );
				} );

				result.should.be.Object().which.has.size( 3 ).and.has.properties( "resultStatus", "resultHeaders", "resultBody" );
				result.resultStatus.should.be.equal( 400 );
				result.resultHeaders.should.be.Object().which.has.property( "content-type" ).which.startWith( "application/json" );
				result.resultBody.should.be.Object().which.has.size( 1 ).and.has.properties( "error" );
				result.resultBody.error.should.be.String();
			} );

			it( "handles request for unknown endpoint", async() => {
				const result = await new Promise( ( resolve, reject ) => {
					const socket = io( ctx.url + "/hitchy" );

					socket.once( "connect_error", reject );

					socket.once( "connect", () => {
						socket.emit( "request", {
							method: "POST",
							url: "/foo/unknown",
							headers: {},
							body: { input: "bar" },
						}, res => {
							resolve( {
								resultStatus: res.statusCode,
								resultHeaders: res.headers,
								resultBody: res.body,
							} );
						} );
					} );
				} );

				result.should.be.Object().which.has.size( 3 ).and.has.properties( "resultStatus", "resultHeaders", "resultBody" );
				result.resultStatus.should.be.equal( 404 );
				result.resultHeaders.should.be.Object();
				result.resultBody.should.be.Object().which.has.property( "error" ).which.is.a.String().and.is.not.empty();
			} );
		} );

		it( "transmits server-side broadcast events to a connected client", async() => {
			const data = await new Promise( ( resolve, reject ) => {
				const socket = io( ctx.url );

				socket.once( "foo-to-all", resolve );
				socket.once( "connect_error", reject );

				socket.once( "connect", () => {
					ctx.hitchy.api.websocket.emit( "foo-to-all", { message: "foo!" } );
				} );
			} );

			data.should.be.Object().which.has.size( 1 ).and.has.property( "message" ).which.is.a.String().and.is.equal( "foo!" );
		} );

		it( "exposes client implementation", async() => {
			const response = await ctx.get( "/socket.io/socket.io.js" );

			response.statusCode.should.be.equal( 200 );
			response.headers.should.have.property( "content-type" ).which.match( /^application\/javascript($|;)/ );
			response.body.should.be.instanceOf( Buffer );
		} );
	} );

	describe( "when operated on a dedicated socket", () => {
		const ctx = {};

		after( SDT.after( ctx ) );
		before( SDT.before( ctx, {
			plugin: true,
			options: {
				// debug: true,
			},
			files: {
				"config/socket.js": `exports.socket = {
					port: 12345,
				}`,
				"config/routes.js": `exports.routes = { 
					"ALL /foo/bar": async( req, res ) => res.json( { requestMethod: req.method, requestBody: await req.fetchBody(), requestHeaders: req.headers } ),
				};`,
			}
		} ) );

		before( () => {
			ctx.url = `http://127.0.0.1:12345`;
		} );


		it( "is exposed at api.websocket", () => {
			ctx.hitchy.api.should.have.property( "websocket" );
			ctx.hitchy.api.websocket.should.be.Object();
		} );

		it( "is exposed at api.services.SocketServer.io", () => {
			ctx.hitchy.api.services.should.have.property( "SocketServer" );
			ctx.hitchy.api.websocket.should.be.equal( ctx.hitchy.api.services.SocketServer.io );
		} );

		it( "looks like the API of socket.io", () => {
			ctx.hitchy.api.websocket.on.should.be.Function();
			ctx.hitchy.api.websocket.of.should.be.Function();
			ctx.hitchy.api.websocket.socketsJoin.should.be.Function();
			ctx.hitchy.api.websocket.socketsLeave.should.be.Function();
			ctx.hitchy.api.websocket.serverSideEmit.should.be.Function();
		} );

		it( "accepts connection", async() => {
			await new Promise( ( resolve, reject ) => {
				const socket = io( ctx.url );

				socket.once( "connect_error", reject );
				socket.once( "connect", resolve );
			} );
		} );

		describe( "dispatches requests received via websocket which", () => {
			it( "succeeds", async() => {
				const result = await new Promise( ( resolve, reject ) => {
					const socket = io( ctx.url + "/hitchy" );

					socket.once( "connect_error", reject );

					socket.once( "connect", () => {
						socket.emit( "request", {
							method: "POST",
							url: "/foo/bar",
							headers: {},
							body: { input: "bar" },
						}, res => {
							resolve( {
								resultStatus: res.statusCode,
								resultHeaders: res.headers,
								resultBody: res.body,
							} );
						} );
					} );
				} );

				result.should.be.Object().which.has.size( 3 ).and.has.properties( "resultStatus", "resultHeaders", "resultBody" );
				result.resultStatus.should.be.equal( 200 );
				result.resultHeaders.should.be.Object().which.has.property( "content-type" ).which.startWith( "application/json" );
				result.resultBody.should.be.Object().which.has.size( 3 ).and.has.properties( "requestMethod", "requestBody", "requestHeaders" );
				result.resultBody.requestMethod.should.be.String().which.is.equal( "POST" );
				result.resultBody.requestBody.should.be.Object().which.has.size( 1 ).and.has.property( "input" ).which.is.a.String().and.is.equal( "bar" );
			} );

			it( "passes custom request headers", async() => {
				const result = await new Promise( ( resolve, reject ) => {
					const socket = io( ctx.url + "/hitchy" );

					socket.once( "connect_error", reject );

					socket.once( "connect", () => {
						socket.emit( "request", {
							method: "POST",
							url: "/foo/bar",
							headers: {
								fOo: "text/json",
							},
							body: { input: "bar" },
						}, res => {
							resolve( {
								resultStatus: res.statusCode,
								resultHeaders: res.headers,
								resultBody: res.body,
							} );
						} );
					} );
				} );

				result.should.be.Object().which.has.size( 3 ).and.has.properties( "resultStatus", "resultHeaders", "resultBody" );
				result.resultStatus.should.be.equal( 200 );
				result.resultHeaders.should.be.Object().which.has.property( "content-type" ).which.startWith( "application/json" );
				result.resultBody.should.be.Object().which.has.size( 3 ).and.has.properties( "requestMethod", "requestBody", "requestHeaders" );
				result.resultBody.requestMethod.should.be.String().which.is.equal( "POST" );
				result.resultBody.requestBody.should.be.Object().which.has.size( 1 ).and.has.property( "input" ).which.is.a.String().and.is.equal( "bar" );
				result.resultBody.requestHeaders.should.be.Object().which.has.property( "foo" ).which.is.a.String().and.is.equal( "text/json" );
			} );

			it( "fails on using unsupported HTTP method", async() => {
				const result = await new Promise( ( resolve, reject ) => {
					const socket = io( ctx.url + "/hitchy" );

					socket.once( "connect_error", reject );

					socket.once( "connect", () => {
						socket.emit( "request", {
							method: "PROPFIND",
							url: "/foo/bar",
							headers: {},
							body: { input: "bar" },
						}, res => {
							resolve( {
								resultStatus: res.statusCode,
								resultHeaders: res.headers,
								resultBody: res.body,
							} );
						} );
					} );
				} );

				result.should.be.Object().which.has.size( 3 ).and.has.properties( "resultStatus", "resultHeaders", "resultBody" );
				result.resultStatus.should.be.equal( 400 );
				result.resultHeaders.should.be.Object().which.has.property( "content-type" ).which.startWith( "application/json" );
				result.resultBody.should.be.Object().which.has.size( 1 ).and.has.properties( "error" );
				result.resultBody.error.should.be.String();
			} );

			it( "fails on providing remote URL to fetch", async() => {
				const result = await new Promise( ( resolve, reject ) => {
					const socket = io( ctx.url + "/hitchy" );

					socket.once( "connect_error", reject );

					socket.once( "connect", () => {
						socket.emit( "request", {
							method: "POST",
							url: "https://google.com/foo/bar",
							headers: {},
							body: { input: "bar" },
						}, res => {
							resolve( {
								resultStatus: res.statusCode,
								resultHeaders: res.headers,
								resultBody: res.body,
							} );
						} );
					} );
				} );

				result.should.be.Object().which.has.size( 3 ).and.has.properties( "resultStatus", "resultHeaders", "resultBody" );
				result.resultStatus.should.be.equal( 400 );
				result.resultHeaders.should.be.Object().which.has.property( "content-type" ).which.startWith( "application/json" );
				result.resultBody.should.be.Object().which.has.size( 1 ).and.has.properties( "error" );
				result.resultBody.error.should.be.String();
			} );

			it( "fails on providing invalid header", async() => {
				const result = await new Promise( ( resolve, reject ) => {
					const socket = io( ctx.url + "/hitchy" );

					socket.once( "connect_error", reject );

					socket.once( "connect", () => {
						socket.emit( "request", {
							method: "POST",
							url: "/foo/bar",
							headers: "some header",
							body: { input: "bar" },
						}, res => {
							resolve( {
								resultStatus: res.statusCode,
								resultHeaders: res.headers,
								resultBody: res.body,
							} );
						} );
					} );
				} );

				result.should.be.Object().which.has.size( 3 ).and.has.properties( "resultStatus", "resultHeaders", "resultBody" );
				result.resultStatus.should.be.equal( 400 );
				result.resultHeaders.should.be.Object().which.has.property( "content-type" ).which.startWith( "application/json" );
				result.resultBody.should.be.Object().which.has.size( 1 ).and.has.properties( "error" );
				result.resultBody.error.should.be.String();
			} );

			it( "fails on providing regular object as payload combined with individual content-type request header", async() => {
				const result = await new Promise( ( resolve, reject ) => {
					const socket = io( ctx.url + "/hitchy" );

					socket.once( "connect_error", reject );

					socket.once( "connect", () => {
						socket.emit( "request", {
							method: "POST",
							url: "/foo/bar",
							headers: {
								"content-type": "application/octet-stream"
							},
							body: { input: "bar" },
						}, res => {
							resolve( {
								resultStatus: res.statusCode,
								resultHeaders: res.headers,
								resultBody: res.body,
							} );
						} );
					} );
				} );

				result.should.be.Object().which.has.size( 3 ).and.has.properties( "resultStatus", "resultHeaders", "resultBody" );
				result.resultStatus.should.be.equal( 400 );
				result.resultHeaders.should.be.Object().which.has.property( "content-type" ).which.startWith( "application/json" );
				result.resultBody.should.be.Object().which.has.size( 1 ).and.has.properties( "error" );
				result.resultBody.error.should.be.String();
			} );

			it( "handles request for unknown endpoint", async() => {
				const result = await new Promise( ( resolve, reject ) => {
					const socket = io( ctx.url + "/hitchy" );

					socket.once( "connect_error", reject );

					socket.once( "connect", () => {
						socket.emit( "request", {
							method: "POST",
							url: "/foo/unknown",
							headers: {},
							body: { input: "bar" },
						}, res => {
							resolve( {
								resultStatus: res.statusCode,
								resultHeaders: res.headers,
								resultBody: res.body,
							} );
						} );
					} );
				} );

				result.should.be.Object().which.has.size( 3 ).and.has.properties( "resultStatus", "resultHeaders", "resultBody" );
				result.resultStatus.should.be.equal( 404 );
				result.resultHeaders.should.be.Object();
				result.resultBody.should.be.Object().which.has.property( "error" ).which.is.a.String().and.is.not.empty();
			} );
		} );

		it( "transmits server-side broadcast events to a connected client", async() => {
			const data = await new Promise( ( resolve, reject ) => {
				const socket = io( ctx.url );

				socket.once( "foo-to-all", resolve );
				socket.once( "connect_error", reject );

				socket.once( "connect", () => {
					ctx.hitchy.api.websocket.emit( "foo-to-all", { message: "foo!" } );
				} );
			} );

			data.should.be.Object().which.has.size( 1 ).and.has.property( "message" ).which.is.a.String().and.is.equal( "foo!" );
		} );

		it( "exposes client implementation", async() => {
			const result = await new Promise( ( resolve, reject ) => {
				const request = HTTP.request( {
					hostname: "127.0.0.1",
					port: 12345,
					path: "/socket.io/socket.io.js",
				}, res => {
					const chunks = [];

					res.once( "error", reject );
					res.on( "data", chunk => {
						chunks.push( chunk );
					} );
					res.on( "end", () => {
						resolve( {
							statusCode: res.statusCode,
							headers: res.headers,
							body: Buffer.concat( chunks ).toString( "utf8" ),
						} );
					} );
				} );

				request.once( "error", reject );
				request.end();
			} );

			result.statusCode.should.be.equal( 200 );
			result.headers.should.have.property( "content-type" ).which.match( /^application\/javascript($|;)/ );
			result.body.should.be.String();
		} );
	} );
} );

"use strict";

/**
 * Integrates this plugin with Hitchy core.
 *
 * @return {Hitchy.Core.PluginAPI} plugin's API
 */
module.exports = function() {
	const api = this;

	return {
		onExposed() {
			const server = api.services.SocketServer;

			Object.defineProperty( api, "websocket", {
				get: () => server.io,
				enumerable: true,
			} );
		},

		initialize() {
			api.services.SocketServer.start();
		},
	};
};
